﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie.Interfaces
{
    public interface IBaseForm
    {
        // Every form has an area, so here we define that in every sub-class
        // must be a method which returns the individually calculated value
        float GetArea();
        // Same as with the area... !! Perimeter = UMFANG
        float GetPerimeter();
        // Pretty Stringifier
        string GetAllVariables();
        // Pretty Stringifier
        string GetAllFormulas();
        // Combines all pretty stringifiers
        string ToString();
    }
}

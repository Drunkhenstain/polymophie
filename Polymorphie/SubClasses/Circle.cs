﻿using Polymorphie.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie.SubClasses
{
    public class Circle : BaseForm
    {
        public float _Radius;
        public float Diameter
        {
            get
            {
                return _Radius * 2;
            }
            set
            {
                _Radius = value / 2;
            }
        }
        public Circle(float radius)
        {
            _FormType = EN_FormType.CIRCLE;
            _Radius = radius;
        }

        public override float GetArea()
        {
            return _Radius * _Radius * (float)Math.PI;
        }

        public override float GetPerimeter()
        {
            return 2 * _Radius * (float)Math.PI;
        }

        public override string ToString()
        {
            return BuildString(_FormType.ToString(),
                         " Area: ",
                         GetArea().ToString(),
                         " Perimeter: ",
                         GetPerimeter().ToString());
        }

        public override string GetAllVariables()
        {
            throw new NotImplementedException();
        }

        public override string GetAllFormulas()
        {
            throw new NotImplementedException();
        }
    }
}

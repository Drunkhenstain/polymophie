﻿using Polymorphie.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie.SubClasses
{
    public class Square : BaseForm
    {
        public float SideA;
        
        public Square(float a)
        {
            _FormType = EN_FormType.SQUARE;
            SideA = a;
        }

        public override string GetAllFormulas()
        {
            throw new NotImplementedException();
        }

        public override string GetAllVariables()
        {
            throw new NotImplementedException();
        }

        public override float GetArea()
        {
            return SideA * SideA;
        }
        public override float GetPerimeter()
        {
            return 4 * SideA;
        }

        public override string ToString()
        {
            return BuildString(_FormType.ToString(),
                         " Area: ",
                         GetArea().ToString(),
                         " Perimeter: ",
                         GetPerimeter().ToString());
        }
    }
}

﻿using Polymorphie.BaseClasses;
using Polymorphie.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie.SubClasses
{
    public class Rectangle : BaseForm
    {
        public float SideA;
        public float SideB;
        public Rectangle(float sideA, float sideB)
        {
            _FormType = EN_FormType.SQUARE;
            SideA = sideA;
            SideB = sideB;
        }

        public override string GetAllFormulas()
        {
            throw new NotImplementedException();
        }

        public override string GetAllVariables()
        {
            throw new NotImplementedException();
        }

        public override float GetArea()
        {
            return SideA * SideB;
        }
        public override float GetPerimeter()
        {
            return 2 * (SideA + SideB);
        }

        public override string ToString()
        {
            return BuildString(_FormType.ToString(),
                         " Area: ",
                         GetArea().ToString(),
                         " Perimeter: ",
                         GetPerimeter().ToString());
        }
    }
}

﻿using Polymorphie.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie.SubClasses
{
    public class RightTriangle : BaseTriangle
    {
        public RightTriangle(float sideA, float sideB, float sideC, float angleA,float angleB)
        {
            _FormType = EN_FormType.RIGHTTRIANGLE;
            _AngleA = angleA;
            _AngleB = angleB;

            _SideA = sideA;
            _SideB = sideB;
            _SideC = sideC;

            _AngleC = 90;
        }

        public override string GetAllFormulas()
        {
            throw new NotImplementedException();
        }

        public override string GetAllVariables()
        {
            throw new NotImplementedException();
        }

        public override float GetArea()
        {
            return (_SideA * _SideB) / 2;
        }

        public override float GetPerimeter()
        {
            return _SideA + _SideB + _SideC;
        }

        public override string ToString()
        {
            return BuildString(_FormType.ToString(),
                         " Area: ",
                         GetArea().ToString(),
                         " Perimeter: ",
                         GetPerimeter().ToString());
        }
    }
}

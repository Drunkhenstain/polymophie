﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie
{
    public enum EN_FormType
    {
        CIRCLE = 1,
        SQUARE = 2,
        RECTANGLE = 3,
        RIGHTTRIANGLE = 4,
    }
}

﻿using Polymorphie.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie.BaseClasses
{
    public abstract class BaseForm : IBaseForm
    {
        protected EN_FormType _FormType;
        public EN_FormType GetFormType { get { return _FormType; } }
        public abstract float GetArea();
        public abstract float GetPerimeter();
        public abstract override string ToString();
        public abstract string GetAllVariables();
        public abstract string GetAllFormulas();
        protected string BuildString(params string[] strings)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in strings)
            {
                sb.Append(s);
            }
            return sb.ToString();
        }
    }
}

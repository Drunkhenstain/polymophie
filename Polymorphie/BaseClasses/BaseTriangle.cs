﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie.BaseClasses
{
    public abstract class BaseTriangle : BaseForm
    {
        public float _SideA;
        public float _SideB;
        public float _SideC;

        public float _AngleA;
        public float _AngleB;
        public float _AngleC;
    }
}

﻿using Polymorphie.BaseClasses;
using Polymorphie.SubClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphie
{
    class Program
    {
        static void Main(string[] args)
        {
            List<BaseForm> formList = new List<BaseForm>();
            Square s1 = new Square(33.33f);
            formList.Add(s1);
            Rectangle r1 = new Rectangle(10.2f, 4.3f);
            formList.Add(r1);
            Circle c1 = new Circle(24.5f);
            formList.Add(c1);
            RightTriangle rt1 = new RightTriangle(17.3f, 11.2f, 0, 0, 0);
            formList.Add(rt1);
            foreach (BaseForm f in formList)
            {
                Console.WriteLine(f.ToString());
                Console.WriteLine("########################");
            }

            Console.ReadLine();
        }
    }
}
